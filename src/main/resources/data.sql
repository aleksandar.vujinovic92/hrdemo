insert into EMPLOYEE values (123456,'Mirko');
insert into EMPLOYEE values (987654,'Predrag');
insert into EMPLOYEE values (654321,'Petar');
insert into EMPLOYEE values (321654,'Vojislav');

insert into TEAM values (1, 'Development', 123456);

insert into EMPLOYEE_TEAM values (1,123456);
insert into EMPLOYEE_TEAM values (1,987654);
insert into EMPLOYEE_TEAM values (1,654321);
insert into EMPLOYEE_TEAM values (1,321654);
