package com.hyperoptic.hrdemo.model;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class EmployeeDto {
    private Long employeeId;
    private String employeeName;
    private List<String> teams;
}
