package com.hyperoptic.hrdemo.model;

import lombok.*;

import java.util.Set;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class TeamDto {
    private Long teamId;
    private String teamName;
    private String teamLead;
    private Set<String> teamMembers;
}
