package com.hyperoptic.hrdemo.model;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class NewTeamDto {
    private String teamName;
    private String teamLead;
    private List<String> members;
}
