package com.hyperoptic.hrdemo.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long teamId;

    @Column(unique = true)
    private String teamName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "teamLeadId")
    private Employee teamLead;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "Employee_Team",
            joinColumns = { @JoinColumn(name = "teamId") },
            inverseJoinColumns = { @JoinColumn(name = "employeeId") })
    private Set<Employee> members;
}
