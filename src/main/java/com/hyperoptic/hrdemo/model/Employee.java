package com.hyperoptic.hrdemo.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;

    @Column(unique = true)
    private String employeeName;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "Employee_Team",
            joinColumns = { @JoinColumn(name = "employeeId") },
            inverseJoinColumns = { @JoinColumn(name = "teamId") }
    )
    private Set<Team> teams;
}
