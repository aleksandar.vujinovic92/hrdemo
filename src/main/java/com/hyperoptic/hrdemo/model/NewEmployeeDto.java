package com.hyperoptic.hrdemo.model;

import lombok.*;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter
public class NewEmployeeDto {
    private String employeeName;
    private String team;
}
