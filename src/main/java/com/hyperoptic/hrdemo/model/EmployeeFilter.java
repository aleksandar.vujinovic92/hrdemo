package com.hyperoptic.hrdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeFilter {
    private Long employeeId;
    private String employeeName;
    private String teamName;
}
