package com.hyperoptic.hrdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TeamFilter {
    private Long teamId;
    private String teamName;
    private String teamLeadName;
}
