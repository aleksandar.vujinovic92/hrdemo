package com.hyperoptic.hrdemo.mapper;

import com.hyperoptic.hrdemo.model.Employee;
import com.hyperoptic.hrdemo.model.EmployeeDto;
import org.mapstruct.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    default EmployeeDto toDto(Employee employee) {
        if (employee == null) {
            return null;
        }
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setEmployeeId(employee.getEmployeeId());
        employeeDto.setEmployeeName(employee.getEmployeeName());
        if (!CollectionUtils.isEmpty(employee.getTeams())) {
            employeeDto.setTeams(employee.getTeams().stream().map(t->t.getTeamName()).collect(Collectors.toList()));
        }
        return employeeDto;
    }
}
