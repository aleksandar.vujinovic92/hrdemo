package com.hyperoptic.hrdemo.mapper;

import com.hyperoptic.hrdemo.model.Team;
import com.hyperoptic.hrdemo.model.TeamDto;
import org.mapstruct.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface TeamMapper {
    default TeamDto toDto(Team team) {
        if (team == null) {
            return null;
        }
        TeamDto teamDto = new TeamDto();

        teamDto.setTeamId(team.getTeamId());

        if (team.getTeamLead() != null) {
            teamDto.setTeamLead(team.getTeamLead().getEmployeeName());
        }

        teamDto.setTeamName(team.getTeamName());
        if (!CollectionUtils.isEmpty(team.getMembers())) {
            teamDto.setTeamMembers(team.getMembers().stream().map(m -> m.getEmployeeName()).collect(Collectors.toSet()));
        }
        return teamDto;
    }

}
