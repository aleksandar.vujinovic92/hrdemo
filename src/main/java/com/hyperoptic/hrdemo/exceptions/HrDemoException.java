package com.hyperoptic.hrdemo.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HrDemoException {
    private String errorCode;
    private String errorMessage;
    private String errorName;
}
