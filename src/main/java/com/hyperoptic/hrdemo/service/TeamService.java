package com.hyperoptic.hrdemo.service;

import com.hyperoptic.hrdemo.exceptions.EmployeeNotFoundException;
import com.hyperoptic.hrdemo.exceptions.TeamNotFoundException;
import com.hyperoptic.hrdemo.mapper.TeamMapper;
import com.hyperoptic.hrdemo.model.*;
import com.hyperoptic.hrdemo.repository.EmployeeRepository;
import com.hyperoptic.hrdemo.repository.TeamRepository;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
@Transactional
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TeamMapper teamMapper;

    public List<TeamDto> getTeam(TeamFilter teamFilter) {
        Iterable<Team> teamsIterable = getFilteredTeams(teamFilter);
        List<TeamDto> teamDtoList = new ArrayList<>();

        for (Team team : teamsIterable) {
            teamDtoList.add(teamMapper.toDto(team));
        }
        return teamDtoList;
    }

    private Iterable<Team> getFilteredTeams(TeamFilter teamFilter) {
        QTeam qTeam = QTeam.team;
        List<Predicate> predicates = new ArrayList<>();

        if (teamFilter.getTeamId() != null) {
            predicates.add(qTeam.teamId.eq(teamFilter.getTeamId()));
        }
        if (!StringUtils.isEmpty(teamFilter.getTeamName())) {
            predicates.add(qTeam.teamName.eq(teamFilter.getTeamName()));
        }

        if (!StringUtils.isEmpty(teamFilter.getTeamLeadName())) {
            predicates.add(qTeam.teamLead.employeeName.eq(teamFilter.getTeamLeadName()));
        }
        Iterable<Team> teamsIterable;
        if (!predicates.isEmpty()) {
            teamsIterable = teamRepository.findAll(ExpressionUtils.allOf(predicates));
        } else {
            teamsIterable = teamRepository.findAll();
        }
        return teamsIterable;
    }

    public void addTeam(NewTeamDto newTeamDto) {
        Assert.notNull(newTeamDto, "TeamDto has to be provided!");
        Assert.notNull(newTeamDto.getTeamName(), "Team name has to be provided on creation!");

        Team newTeam = new Team();
        newTeam.setTeamName(newTeamDto.getTeamName());
        if (!StringUtils.isEmpty(newTeamDto.getTeamLead())) {
            Optional<Employee> teamLeadOptional = employeeRepository.findByEmployeeName(newTeamDto.getTeamLead());
            if (!teamLeadOptional.isPresent()) {
                throw new EmployeeNotFoundException();
            } else {
                newTeam.setTeamLead(teamLeadOptional.get());
            }
        }

        if (!CollectionUtils.isEmpty(newTeamDto.getMembers())) {
            Set<Employee> members = new HashSet<>();
            for (String memberName : newTeamDto.getMembers()) {
                Optional<Employee> memberOptional = employeeRepository.findByEmployeeName(memberName);
                if (!memberOptional.isPresent()) {
                    throw new EmployeeNotFoundException();
                } else {
                    members.add(memberOptional.get());
                }
            }
            newTeam.setMembers(members);
        }

        teamRepository.save(newTeam);
    }

    public void updateTeam(TeamDto teamDto) {
        assertParameters(teamDto);

        Optional<Team> teamOptional = teamRepository.findById(teamDto.getTeamId());

        if (teamOptional.isPresent()) {
            Team dbTeam = teamOptional.get();
            if (!StringUtils.isEmpty(teamDto.getTeamName())) {
                dbTeam.setTeamName(teamDto.getTeamName());
            }

            if (!StringUtils.isEmpty(teamDto.getTeamLead())) {
                Optional<Employee> teamLeadOptional = employeeRepository.findByEmployeeName(teamDto.getTeamLead());
                if (!teamLeadOptional.isPresent()) {
                    throw new EmployeeNotFoundException();
                } else {
                    dbTeam.setTeamLead(teamLeadOptional.get());
                }
            }

            if (!CollectionUtils.isEmpty(teamDto.getTeamMembers())) {
                dbTeam.getMembers().clear();
                for (String memberName : teamDto.getTeamMembers()) {
                    Optional<Employee> newTeamMemberOptional = employeeRepository.findByEmployeeName(memberName);
                    if (!newTeamMemberOptional.isPresent()) {
                        if (!memberName.equals(""))
                            throw new EmployeeNotFoundException();
                    } else {
                        dbTeam.getMembers().add(newTeamMemberOptional.get());
                    }
                }
            }

            teamRepository.save(dbTeam);
        } else {
            throw new TeamNotFoundException();
        }
    }

    private void assertParameters(TeamDto teamDto) {
        Assert.notNull(teamDto, "TeamDto has to be provided!");
        Assert.notNull(teamDto.getTeamId(), "TeamId value has to be provided!");
        Assert.isTrue(teamDto.getTeamId() > 0, "TeamId value has to be greater than 0!");
    }

    public void deleteTeamById(long teamId) {
        Assert.notNull(teamId, "TeamId has to be provided for deleting!");
        teamRepository.deleteById(teamId);
    }
}

