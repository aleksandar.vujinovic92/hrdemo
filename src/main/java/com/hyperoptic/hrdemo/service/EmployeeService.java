package com.hyperoptic.hrdemo.service;

import com.google.common.collect.Sets;
import com.hyperoptic.hrdemo.exceptions.EmployeeNotFoundException;
import com.hyperoptic.hrdemo.exceptions.TeamNotFoundException;
import com.hyperoptic.hrdemo.mapper.EmployeeMapper;
import com.hyperoptic.hrdemo.model.*;
import com.hyperoptic.hrdemo.repository.EmployeeRepository;
import com.hyperoptic.hrdemo.repository.TeamRepository;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private EntityManager entityManager;

    public List<EmployeeDto> getEmployee(EmployeeFilter employeeFilter) {
        if (employeeFilter.getEmployeeId() != null) {
            Assert.isTrue(employeeFilter.getEmployeeId() > 0, "EmployeeId value has to be greater than 0!");
        }

        Iterable<Employee> employees = getFilteredEmployees(employeeFilter);
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        for (Employee employee : employees) {
            employeeDtoList.add(employeeMapper.toDto(employee));
        }
        return employeeDtoList;
    }

    private Iterable<Employee> getFilteredEmployees(EmployeeFilter employeeFilter) {
        QEmployee qEmployee = QEmployee.employee;
        List<Predicate> predicates = new LinkedList<>();

        if (employeeFilter.getEmployeeId() != null) {
            predicates.add(qEmployee.employeeId.eq(employeeFilter.getEmployeeId()));
        }
        if (!StringUtils.isEmpty(employeeFilter.getEmployeeName())) {
            predicates.add(qEmployee.employeeName.eq(employeeFilter.getEmployeeName()));
        }

        if (employeeFilter.getTeamName() != null) {
            predicates.add(qEmployee.teams.any().teamName.eq(employeeFilter.getTeamName()));
        }
        Iterable<Employee> employees;
        if (!predicates.isEmpty()) {
            employees = employeeRepository.findAll(ExpressionUtils.allOf(predicates));
        } else {
            employees = employeeRepository.findAll();
        }
        return employees;
    }

    public void addEmployee(NewEmployeeDto newEmployeeDto) {
        Assert.notNull(newEmployeeDto, "EmployeeDto has to be provided!");
        Assert.notNull(newEmployeeDto.getEmployeeName(), "Employee name has to be provided on creation!");

        Employee newEmployee = new Employee();
        newEmployee.setEmployeeName(newEmployeeDto.getEmployeeName());
        if (!StringUtils.isEmpty(newEmployeeDto.getTeam())) {
            Optional<Team> teamOptional = teamRepository.findByTeamName(newEmployeeDto.getTeam());
            if (!teamOptional.isPresent()) {
                throw new TeamNotFoundException();
            } else {
                newEmployee.setTeams(Sets.newHashSet(teamOptional.get()));
            }
        }

        employeeRepository.save(newEmployee);
    }

    public void updateEmployee(EmployeeDto employeeDto) {
        Assert.notNull(employeeDto, "EmployeeDto has to be provided!");
        Assert.notNull(employeeDto.getEmployeeId(), "EmployeeId value has to be provided!");
        Assert.isTrue(employeeDto.getEmployeeId() > 0, "EmployeeId value has to be greater than 0!");

        Optional<Employee> employeeOptional = employeeRepository.findById(employeeDto.getEmployeeId());

        if (employeeOptional.isPresent()) {
            Employee dbEmployee = employeeOptional.get();
            if (!StringUtils.isEmpty(employeeDto.getEmployeeName())) {
                dbEmployee.setEmployeeName(employeeDto.getEmployeeName());
            }

            if (!CollectionUtils.isEmpty(employeeDto.getTeams())) {
                dbEmployee.getTeams().clear();

                for (String teamName : employeeDto.getTeams()) {
                    Optional<Team> teamOptional = teamRepository.findByTeamName(teamName);
                    if (!teamOptional.isPresent()) {
                        if (!teamName.equals("")) {
                            throw new TeamNotFoundException();
                        }
                    } else {
                        dbEmployee.getTeams().add(teamOptional.get());
                    }
                }
            }

            employeeRepository.save(dbEmployee);
        } else {
            throw new EmployeeNotFoundException();
        }
    }

    public void deleteEmployeeById(long employeeId) {
        Assert.notNull(employeeId, "EmployeeId has to be provided!");
        Assert.isTrue(employeeId > 0, "EmployeeId value has to be greater than 0!");

        employeeRepository.deleteById(employeeId);
    }
}
