package com.hyperoptic.hrdemo.controller;

import com.hyperoptic.hrdemo.ResponseEntityConverter;
import com.hyperoptic.hrdemo.model.EmployeeDto;
import com.hyperoptic.hrdemo.model.EmployeeFilter;
import com.hyperoptic.hrdemo.model.NewEmployeeDto;
import com.hyperoptic.hrdemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "/employee")
    public ResponseEntity<List<EmployeeDto>> getEmployee(
            @RequestParam(required = false) Long employeeId,
            @RequestParam(required = false) String employeeName,
            @RequestParam(required = false) String teamName) {
        try {
            EmployeeFilter employeeFilter = new EmployeeFilter(employeeId, employeeName, teamName);
            List<EmployeeDto> employeeDtoList = employeeService.getEmployee(employeeFilter);
            return new ResponseEntity<>(employeeDtoList, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);

        }
    }

    @PostMapping(value = "/employee",
            consumes = "application/json")
    public ResponseEntity<Void> addEmployee(@RequestBody NewEmployeeDto newEmployeeDto) {
        try {
            employeeService.addEmployee(newEmployeeDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);
        }
    }

    @PutMapping(value = "/employee",
            consumes = "application/json")
    public ResponseEntity<Void> updateEmployee(@RequestBody EmployeeDto employeeDto) {
        try {
            employeeService.updateEmployee(employeeDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);
        }
    }

    @DeleteMapping("/employee/{employeeId}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable("employeeId") long employeeId) {
        try {
            employeeService.deleteEmployeeById(employeeId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);
        }
    }
}
