package com.hyperoptic.hrdemo.controller;

import com.hyperoptic.hrdemo.ResponseEntityConverter;
import com.hyperoptic.hrdemo.model.NewTeamDto;
import com.hyperoptic.hrdemo.model.TeamDto;
import com.hyperoptic.hrdemo.model.TeamFilter;
import com.hyperoptic.hrdemo.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping(value = "/team")
    public ResponseEntity<List<TeamDto>> getTeam(
            @RequestParam(required = false) Long teamId,
            @RequestParam(required = false) String teamName,
            @RequestParam(required = false) String teamLeadName) {
        try {
            TeamFilter teamFilter = new TeamFilter(teamId, teamName, teamLeadName);
            List<TeamDto> teamDtoList = teamService.getTeam(teamFilter);
            return new ResponseEntity<>(teamDtoList, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);
        }
    }

    @PostMapping(value = "/team",
            consumes = "application/json")
    public ResponseEntity<Void> createTeam(@RequestBody NewTeamDto newTeamDto) {
        try {
            teamService.addTeam(newTeamDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);
        }
    }

    @PutMapping(value = "/team",
            consumes = "application/json")
    public ResponseEntity<Void> updateTeam(@RequestBody TeamDto teamDto) {
        try {
            teamService.updateTeam(teamDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);

        }
    }

    @DeleteMapping("/team/{teamId}")
    public ResponseEntity<Void> deleteTeam(@PathVariable("teamId") long teamId) {
        try {
            teamService.deleteTeamById(teamId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntityConverter.responseException(e);
        }
    }
}
