package com.hyperoptic.hrdemo.repository;

import com.hyperoptic.hrdemo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long>, QuerydslPredicateExecutor<Employee> {
    Optional<Employee> findByEmployeeName(String teamLead);
}
