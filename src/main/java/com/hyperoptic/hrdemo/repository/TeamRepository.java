package com.hyperoptic.hrdemo.repository;

import com.hyperoptic.hrdemo.model.Team;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TeamRepository extends CrudRepository<Team, Long>, QuerydslPredicateExecutor<Team> {
    Optional<Team> findByTeamName(String teamName);
}
