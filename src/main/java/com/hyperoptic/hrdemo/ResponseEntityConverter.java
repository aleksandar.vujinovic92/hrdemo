package com.hyperoptic.hrdemo;

import com.hyperoptic.hrdemo.exceptions.EmployeeNotFoundException;
import com.hyperoptic.hrdemo.exceptions.HrDemoException;
import com.hyperoptic.hrdemo.exceptions.TeamNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntityConverter {
    private static final String EMPLOYEE_NOT_FOUND = "EmployeeNotFound";
    private static final String TEAM_NOT_FOUND = "TeamNotFound";
    private static final String WRONG_DB_INPUT = "WrongDbInput";
    private static final String WRONG_ENTITY_ID = "WrongEntityId";

    @SuppressWarnings("unchecked")
    public static <R> ResponseEntity<R> responseException(Exception rootException) {
        HrDemoException hrDemoException = new HrDemoException();

        if (EmployeeNotFoundException.class.isAssignableFrom(rootException.getClass())) {
            hrDemoException.setErrorName(EMPLOYEE_NOT_FOUND);
            hrDemoException.setErrorCode(String.valueOf(HttpStatus.NOT_FOUND));
            return new ResponseEntity<>((R) hrDemoException, HttpStatus.NOT_FOUND);
        }

        if (TeamNotFoundException.class.isAssignableFrom(rootException.getClass())) {
            hrDemoException.setErrorName(TEAM_NOT_FOUND);
            hrDemoException.setErrorCode(String.valueOf(HttpStatus.NOT_FOUND));
            return new ResponseEntity<>((R) hrDemoException, HttpStatus.NOT_FOUND);
        }
        if(DataIntegrityViolationException.class.isAssignableFrom(rootException.getClass())) {
            hrDemoException.setErrorName(WRONG_DB_INPUT);
            hrDemoException.setErrorCode(String.valueOf(HttpStatus.NOT_ACCEPTABLE));
            return new ResponseEntity<>((R) hrDemoException, HttpStatus.NOT_ACCEPTABLE);
        }
        if(EmptyResultDataAccessException.class.isAssignableFrom(rootException.getClass())) {
            hrDemoException.setErrorName(WRONG_ENTITY_ID);
            hrDemoException.setErrorCode(String.valueOf(HttpStatus.NOT_FOUND));
            return new ResponseEntity<>((R) hrDemoException, HttpStatus.NOT_FOUND);

        }

        hrDemoException.setErrorName(rootException.getClass().getSimpleName());
        hrDemoException.setErrorMessage(rootException.getMessage());

        return new ResponseEntity<>((R) hrDemoException, HttpStatus.NOT_ACCEPTABLE);
    }
}
