# HrDemo

Demo HR app

We need an application that will be able to handle that kind of data. 
- It should enable CRUD operation over REST.
- It should have one search REST endpoint with multiple optional filters.
- It should be developed using some relational data base, Spring/Spring boot, Java 8
- It should be basically documented.


DB:
Type: in-memory H2

Tables:

	Table Employee {
		employeeId : integer; PK
		employeeName: varchar;
	}

	table Employee-Team {
		teamId: integer;
		employeeId: integer;

		PK (employeeId,teamId)
	}

	Table Team {
		teamId: integer; PK
		teamName: string;
		teamLead: employeeId; FK n->1
	}


Endpoints:

1. EmployeeController

	GET /employee [optional filters: employeeId, employeeName, teamName]

	POST /employee

	PUT /employee

	DELETE /employee


2. TeamController

	GET /team [optional filters: teamId, teamName, teamLeadName]

	POST /team

	PUT /team

	DELETE /team


URLS:
Swagger: http://localhost:8080/swagger-ui.html 

H2 console: http://localhost:8080/h2-ui [jdbcUrl=jdbc:h2:mem:testdb]
	
